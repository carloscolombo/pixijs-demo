import * as PIXI from 'pixi.js'
import { ActivityPath } from './activity-path';
import { Graphics } from "./graphics";
import { PixiApp } from "src/helpers/pixiApp";
import { ActivityPathHandle } from './path-handle';

export class Activity extends Graphics {
    private _data = null;

    private _type = "activity";

    outgoingPathIds: string[] = [];
    incomingPathIds: string[] = [];

    get type(): string { return this._type; }
    get numberOfOutgoingPaths(): number { return this.outgoingPathIds.length; }
    get numberOfIncomingPaths(): number { return this.incomingPathIds.length; }
    get isDragging() { return this.dragging; }

    constructor(
        public startX: number,
        public startY: number,
        public startWidth: number,
        public startHeight: number
    ) {
        super();

        this.beginFill(0xFFFFFF);
        this.lineStyle(1, 0xE1E1E1, 1);
        this.drawRect(0, 0, startWidth, startHeight);
        this.interactive = true;
        this.x = startX;
        this.y = startY;
        this.endFill();

        this
            .on('pointerdown', this.onDragStart)
            .on('pointerup', this.onDragEnd)
            .on('pointerupoutside', this.onDragEnd)
            .on('pointermove', this.onDragMove)
            .on('dblclick', () => { alert('open') });
    }

    addOutgoingPath(activityPathId: string) {
        let index = this.outgoingPathIds.findIndex(x => x == activityPathId);

        index == -1
            ? this.outgoingPathIds.push(activityPathId)
            : this.outgoingPathIds[index] = activityPathId;
    }

    addIncomingPath(activityPathId: string) {

        let index = this.incomingPathIds.findIndex(x => x == activityPathId);

        if (index == -1)
            this.incomingPathIds.push(activityPathId);

    }

    removeIncomingPath(activityPathId: string) {
        let index = this.incomingPathIds.findIndex(x => x == activityPathId);

        if (index != -1) {
            this.incomingPathIds.splice(index, 1);
        }
    }

    onDragStart = (event) => {
        this._data = event.data;
        this.dragging = true;

        this.showControls();
    }

    onDragEnd = (event) => {
        this.dragging = false;
        this._data = null
        this.showControls();
        // PixiApp.getInstance().pausePlugins('mouse-edges');
    }

    onDragMove = (event) => {
        if (this.dragging) {
            this.hideControls();
            this._data = event.data;
            const newPosition = this._data.getLocalPosition(this.parent);

            this.x = newPosition.x - this.width / 2;
            this.y = newPosition.y - this.height / 2;

            this.outgoingPathIds.forEach((activityPathId: string) => {
                let anchorX = this.x + this.width;
                let anchorY = this.y + this.height / 2;
                let activityPath = PixiApp.getInstance().getObjectById<ActivityPath>(activityPathId);

                if (activityPath) {
                    activityPath.updateLine(anchorX, anchorY, activityPath.head.x, activityPath.head.y);
                }
            });


            this.incomingPathIds.forEach((activityPathId: string) => {
                let anchorX = this.x;
                let anchorY = this.y + this.height / 2;
                let activityPath = PixiApp.getInstance().getObjectById<ActivityPath>(activityPathId);

                if (activityPath) {
                    activityPath.updateLine(activityPath.tail.x, activityPath.tail.y, anchorX, anchorY);

                    let handle = PixiApp
                        .getInstance()
                        .getObjectById<ActivityPathHandle>(activityPath.handleId);

                    if (handle) {
                        handle.x = anchorX;
                        handle.y = anchorY;
                    }
                }
            });

            // PixiApp.getInstance().viewport.moveCenter(this.x, this.y);
        }
    }

    showControls() {
        let pixiApp = PixiApp.getInstance();

        pixiApp.state.next({
            selectedActivity: {
                coordinates: {
                    x: this.x * pixiApp.zoom,
                    y: this.y * pixiApp.zoom,
                },
                size: {
                    width: this.width * pixiApp.zoom,
                    height: this.height * pixiApp.zoom
                },
                id: this.id
            }
        });
    }

    hideControls() {
        PixiApp.getInstance().state.next({});
    }

    updateControls() {
        let pixiApp = PixiApp.getInstance();
        let selectedActivity = pixiApp.state.value.selectedActivity;

        if (selectedActivity && selectedActivity.id == this.id) {
            this.showControls();
        }
    }
}