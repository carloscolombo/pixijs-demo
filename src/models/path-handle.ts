import * as PIXI from 'pixi.js'
import { Subject } from 'rxjs';
import { Coordinates, ActivityPath as string, ActivityPath } from './activity-path';
import { Graphics } from "./graphics";
import { Collision } from 'src/helpers/collision';
import { Injectable, Injector } from '@angular/core';
import { ActivityService } from 'src/services/activity.service';
import { Activity } from './activity';
import { PixiApp } from "src/helpers/pixiApp";
import { Utils } from 'src/helpers/util';


export class ActivityPathHandle extends Graphics {

    private _data = null;

    constructor(x: number, y: number, public pathId: string) {
        super();

        this.beginFill(0xDE3249);
        this.drawCircle(0, 0, 5);
        this.position.x = x;
        this.position.y = y;
        this.visible = true;
        this.interactive = true;

        this
            .on('pointerdown', this.onDragStart)
            .on('pointerup', this.onDragEnd)
            .on('pointerupoutside', this.onDragEnd)
            .on('pointermove', this.onDragMove);

    }

    onDragStart = (event) => {
        this._data = event.data;
        this.dragging = true;
    }

    onDragEnd = (event) => {
        this.dragging = false;
        this._data = null;
    }

    onDragMove = (event) => {
        if (this.dragging) {
            let path = PixiApp.getInstance().getObjectById<ActivityPath>(this.pathId);

            const newPosition = this._data.getLocalPosition(this.parent);

            this.x = newPosition.x;
            this.y = newPosition.y - this.height / 2;

            let headX = this.x;
            let headY = this.y;

            PixiApp
                .getInstance()
                .getObjectsByType<Activity>(Activity)
                .forEach(activity => {
                    // we dont want to attach to ourselves
                    if (activity.outgoingPathIds.indexOf(this.pathId) == -1) {
                        if (Collision.hitTestRectangle(this, activity)) {
                            headX = activity.x;
                            headY = activity.y + activity.height / 2;

                            this.x = headX;
                            this.y = headY;

                            activity.addIncomingPath(this.pathId);
                            path.connectActivity(activity.id);
                        } else {
                            activity.removeIncomingPath(this.pathId);
                            path.connectActivity(null);
                        }
                    }
                });

            path.updateLine(path.tail.x, path.tail.y, headX, headY);

            // PixiApp.getInstance().viewport.moveCenter(this.x, this.y);
        }
    }
}