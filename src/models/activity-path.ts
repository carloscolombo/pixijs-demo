import * as PIXI from 'pixi.js'
import { Activity } from './activity';
import { ActivityPathHandle } from './path-handle';
import { Graphics } from './graphics';

export class ActivityPath extends Graphics {
    private _tail: Coordinates;
    private _head: Coordinates;
    private _handleId: string;
    private _connectedActivityId: string;

    get head() { return this._head; }
    get tail() { return this._tail; }
    get handleId() { return this._handleId; }
    get hasConnection() { return this._connectedActivityId != null; }

    constructor() {
        super();
        this.lineStyle(2, 0x3B95D1)
    }

    connectActivity(id: string) {
        this._connectedActivityId = id;
    }

    drawPath(activity: Activity) {
        let count = activity.numberOfOutgoingPaths;
        let lineLength = 150;

        if (count == 0) {
            // no connections
            this.setTail(activity.x + activity.width, activity.y + (activity.height / 2));
            this.setHead(activity.x + activity.width + lineLength, activity.y + activity.height / 2);

        } else if (count % 2 == 1) {

            // odd connections
            this.setTail(activity.x + activity.width, activity.y + (activity.height / 2));
            this.setHead(activity.x + activity.width + lineLength, activity.y + activity.height * count * 2);
        } else {

            // even connections
            this.setTail(activity.x + activity.width, activity.y + (activity.height / 2));
            this.setHead(activity.x + activity.width + lineLength, activity.y - activity.height * count * 2);
        }
    }



    addHandle(id: string) {
        this._handleId = id;
    }

    updateLine(x1: number, y1: number, x2: number, y2: number) {
        this.clear();
        this.setTail(x1, y1);
        this.setHead(x2, y2);
    }

    setTail(x: number, y: number): void {
        this._tail = {
            x,
            y
        }

        this.moveTo(this._tail.x, this._tail.y)
    }

    setHead(x: number, y: number): void {
        this._head = {
            x,
            y
        }

        let midpointX = Math.abs((this._tail.x + this.head.x) / 2);

        this.bezierCurveTo(midpointX, this._tail.y, midpointX, this._head.y, x, y);
    }
}


export interface Coordinates {
    x: number;
    y: number;
}

export interface Size {
    width: number;
    height: number;
}