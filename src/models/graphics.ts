import uuidv1 from 'uuid/v1';
import * as PIXI from 'pixi.js';
import { PixiApp } from 'src/helpers/pixiApp';


export class Graphics extends PIXI.Graphics {
    public type: string;
    public id: string;
    public dragging: boolean = false;

    constructor() {
        super();
        this.id = uuidv1();
    }
}
