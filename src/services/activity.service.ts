import { Injectable } from '@angular/core';
import * as PIXI from 'pixi.js'
import { BehaviorSubject } from 'rxjs';
import { Activity } from 'src/models/activity';
import { ActivityPath } from 'src/models/activity-path';
import { ActivityPathHandle } from 'src/models/path-handle';
import { PixiApp } from "src/helpers/pixiApp";


@Injectable()
export class ActivityService {
    private _state = new BehaviorSubject(null);
    // private _activityRectList: Activity[] = [];

    constructor() { }

    createIf(x: number, y: number) {
        // draw rect
        let ifRect = new Activity(x, y, 250, 66);

        // draw then paths
        let thenPath = new ActivityPath();
        thenPath.drawPath(ifRect);

        // add line reference to rect
        // TODO: fix we dont have to put in the right order
        ifRect.addOutgoingPath(thenPath.id);

        let elsePath = new ActivityPath();
        elsePath.drawPath(ifRect);

        // add line reference to rect
        ifRect.addOutgoingPath(elsePath.id);

        // draw handles
        let thenHandle = new ActivityPathHandle(thenPath.head.x, thenPath.head.y, thenPath.id);
        let elseHandle = new ActivityPathHandle(elsePath.head.x, elsePath.head.y, elsePath.id);

        // add handles refrence to line
        thenPath.addHandle(thenHandle.id);
        elsePath.addHandle(elseHandle.id);

        //TODO: possibly make into its own class
        // draw icon
        let sprite = PIXI.Sprite.from('assets/arrows.png');
        sprite.y = 15;
        sprite.x = 10;

        // TODO: possibly make into its own class
        // write text

        let text = new PIXI.Text('If statement', { fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'center' });
        text.x = 55;
        text.y = 24;

        ifRect.addChild(sprite, text);

        PixiApp.getInstance().viewport.addChild(ifRect, thenPath, thenHandle, elsePath, elseHandle);
    }
}