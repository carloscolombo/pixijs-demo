export class Utils {
    public static getSafe(fn) {
        try {
            return fn() || null;
        } catch (e) {
            return null;
        }
    }
}