import { Activity } from 'src/models/activity';
import { Coordinates, ActivityPath } from 'src/models/activity-path';
import { Graphics } from "src/models/graphics";
import { ActivityPathHandle } from 'src/models/path-handle';
import { PixiApp } from './pixiApp';
import * as PIXI from 'pixi.js'

export class GroupSelection extends PIXI.Graphics {
    private static instance: GroupSelection;
    private _data: any;
    private _dragging: boolean = false;
    private _selectedObjects: PIXI.DisplayObject[] = [];
    private _relativePositions: Coordinates[] = [];

    get isDragging() { return this._dragging; }

    private constructor() {
        super();
    }

    public static getInstance(): GroupSelection {
        if (!GroupSelection.instance) {
            GroupSelection.instance = new GroupSelection();
            GroupSelection.instance.interactive = true;

            GroupSelection.instance
                .on('pointerdown', GroupSelection.instance._onDragStart)
                .on('pointerup', GroupSelection.instance._onDragEnd)
                .on('pointerupoutside', GroupSelection.instance._onDragEnd)
                .on('pointermove', GroupSelection.instance._onDragMove);

            GroupSelection.instance.beginFill(0x000000);
            GroupSelection.instance.alpha = .15;
            GroupSelection.instance.drawRect(0, 0, 100, 100);
            GroupSelection.instance.endFill();
            GroupSelection.instance.width = 0;
            GroupSelection.instance.height = 0;
            GroupSelection.instance.visible = false;
        }
        return GroupSelection.instance;
    }

    setGrouping(selectedObjects: Graphics[]) {
        this._selectedObjects = selectedObjects;
        if (this._selectedObjects.length > 0) {
            let startX: number, startY: number, endX: number, endY: number, temp: number;
            this._selectedObjects.forEach(object => {
                if (!startX || startX > object.x) {
                    startX = object.x;
                }
                if (!startY || startY > object.y) {
                    startY = object.y;
                }
                temp = object.x + object.getBounds().width;
                if (!endX || (temp > endX)) {
                    endX = temp;
                }
                temp = object.y + object.getBounds().height;
                if (!endY || (temp > endY)) {
                    endY = temp;
                }
            });

            this.x = startX;
            this.y = startY;
            this.width = endX - startX;
            this.height = endY - startY;
            this.visible = true;

            this._selectedObjects.forEach(object => {
                this._relativePositions.push({
                    x: Math.abs(this.x - object.x),
                    y: Math.abs(this.y - object.y)
                });
            });

            PixiApp.getInstance().viewport.addChild(this);
        }
    }

    deselect() {
        this.visible = false;
        this._selectedObjects.forEach(x => x.interactive = true);
        this._selectedObjects = [];
        this._relativePositions = [];
    }

    private _onDragStart = (event) => {
        this._data = event.data;
        this._dragging = true;
        this._selectedObjects
            .forEach((object: Activity) => {
                object.interactive = false;
            });
        console.log('group')
    };

    private _onDragEnd = (event) => {
        this._dragging = false;
        this._data = null;
        // PixiApp.getInstance().pausePlugins('mouse-edges');
    };

    private _onDragMove = (event) => {
        if (this._dragging) {
            const newPosition = this._data.getLocalPosition(this.parent);

            this.x = newPosition.x - this.width / 2;
            this.y = newPosition.y - this.height / 2;

            this._selectedObjects
                .forEach((object: Activity | ActivityPathHandle, index: number) => {
                    object.x = this.x + this._relativePositions[index].x;
                    object.y = this.y + this._relativePositions[index].y;
                    if (object instanceof Activity) {
                        object.outgoingPathIds.forEach(pathId => {
                            let tailX = object.x + object.width;
                            let tailY = object.y + object.height / 2;
                            let path = PixiApp
                                .getInstance()
                                .getObjectById<ActivityPath>(pathId);
                            if (path) {
                                let handle = PixiApp
                                    .getInstance()
                                    .getObjectById<ActivityPathHandle>(path.handleId);
                                let headX = handle.x;
                                let headY = handle.y;
                                path.updateLine(tailX, tailY, headX, headY);
                            }
                        });
                    }
                    if (object instanceof ActivityPathHandle) {
                        let path = PixiApp.getInstance().getObjectById<ActivityPath>(object.pathId);
                        path.updateLine(path.tail.x, path.tail.y, object.x, object.y);
                    }
                });

            // PixiApp.getInstance().viewport.moveCenter(this.x, this.y);
        }
    };
}
