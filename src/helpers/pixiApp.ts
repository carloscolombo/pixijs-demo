import { ActivityPath, Coordinates, Size } from 'src/models/activity-path';
import { Graphics } from "src/models/graphics";
import { Viewport } from 'pixi-viewport';
import { Background } from "./background";
import * as PIXI from 'pixi.js'
import { ActivityPathHandle } from 'src/models/path-handle';
import { Activity } from 'src/models/activity';
import { GroupSelection } from './group';
import { Collision } from './collision';
import { BehaviorSubject } from 'rxjs';

export enum AppState {
    POINTER,
    SELECTOR,
    GRAB
}

export interface PixiAppState {
    selectedActivity?: {
        coordinates: Coordinates
        size: Size,
        id: string
    }
}

export class PixiApp extends PIXI.Application {
    private static instance: PixiApp;

    state = new BehaviorSubject<PixiAppState>({})
    _selectorStartX: number;
    _selectorStartY: number;
    _data: any;
    _dragging: boolean;
    _selector: PIXI.Graphics = new PIXI.Graphics();
    _group: GroupSelection = GroupSelection.getInstance();
    zoom = 1.0;

    viewport: Viewport;
    appState = AppState.POINTER;

    private constructor() {
        super({
            antialias: true,
            backgroundColor: 0xf1f1f1,
            width: window.innerWidth,
            height: window.innerHeight
        });
        let tilingSprite = Background.getInstance();
        this.stage.addChild(tilingSprite);
    }

    public static getInstance(): PixiApp {
        if (!PixiApp.instance) {
            PixiApp.instance = new PixiApp();

            // create viewport
            PixiApp.instance.viewport = new Viewport({
                screenWidth: window.innerWidth,
                screenHeight: window.innerHeight,
                worldWidth: 1000 * 10,
                worldHeight: 1000 * 10,
                interaction: PixiApp.instance.renderer.plugins.interaction // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
            });

            // add the viewport to the stage
            PixiApp.instance.stage.addChild(PixiApp.instance.viewport);

            // set drag bounds
            PixiApp.instance.viewport
                .drag()
                // .mouseEdges({
                //     distance: 100,
                //     speed: 12
                // })
                .clamp({
                    direction: 'all'
                });

            PixiApp.instance.pausePlugins('drag');

            // add selection tool highlighting square
            PixiApp.instance.viewport.on('pointerdown', PixiApp.instance._onDragStart);
            PixiApp.instance.viewport.on('pointerup', PixiApp.instance._onDragEnd);
            PixiApp.instance.viewport.on('pointerupoutside', PixiApp.instance._onDragEnd);
            PixiApp.instance.viewport.on('pointermove', PixiApp.instance._onDragMove);

            PixiApp.instance._group.lineStyle(2, 0x000000, 1);
            PixiApp.instance._selector.beginFill(0x3B95D1);
            PixiApp.instance._selector.alpha = .10;

            PixiApp.instance._selector.lineStyle(1, 0xFBFBFD, 1);
            PixiApp.instance._selector.drawRect(0, 0, 100, 100);
            PixiApp.instance._selector.x = 0;
            PixiApp.instance._selector.y = 0;
            PixiApp.instance._selector.width = 0;
            PixiApp.instance._selector.height = 0;
            PixiApp.instance._selector.endFill();

            PixiApp.instance.viewport.addChild(PixiApp.instance._selector);
        }
        return PixiApp.instance;
    }

    public getObjectsByIds<T>(...ids: string[]): T[] {
        return this.viewport.children.filter((x: Graphics) => ids.includes(x.id)) as any;
    }

    public getObjectById<T>(id: string): T {
        return this.viewport.children.find((x: Graphics) => x.id == id) as any;
    }

    /**
     * Returns objects by type
     *
     * @template T The Type
     * @param {Function} arg The Type
     * @returns {T[]} An array of the provided Type
     * @memberof PixiApp
     */
    public getObjectsByType<T>(arg: Function): T[] {
        return this.viewport.children.filter((graphics: Graphics) => graphics instanceof arg) as any;
    }

    public isAnObjectBeingDragged(): boolean {
        let draggingObjects = this.viewport.children
            .filter((x: Graphics) => x.dragging);

        return draggingObjects.length > 0

    }

    public canSelect() {
        return AppState.POINTER === this.appState && !this.isAnObjectBeingDragged()
    }

    public isGrabbing() {
        return AppState.GRAB === this.appState
    }

    public resumePlugins(...plugins: string[]) {
        plugins.forEach(x => this.viewport.plugins.resume(x));
        this.printPlugins();
    }

    public pausePlugins(...plugins: string[]) {
        plugins.forEach(x => this.viewport.plugins.pause(x));
        this.printPlugins();
    }

    // TODO REMOVE
    // FOR DEBUGING PURPOSES ONLY
    private printPlugins() {
        let keys = Object.keys((<any>this.viewport.plugins).plugins);

        let runningPlugins = keys.filter(x => {
            let plugins = (<any>this.viewport.plugins).plugins;

            return !plugins[x].paused;
        });

        console.log(runningPlugins);
    }

    public setPointerState() {
        this.appState = AppState.POINTER;
        this.pausePlugins('drag');
        this.viewport.children.forEach(x => x.interactive = true);
    }

    public setGrabState() {
        this.appState = AppState.GRAB;
        this.resumePlugins('drag');
        this.viewport.children.forEach(x => x.interactive = false);
    }

    public setSelectorState() {
        this.appState = AppState.SELECTOR;
        this.pausePlugins('drag');
        this.viewport.children.forEach(x => x.interactive = false);
    }

    private _onDragStart = (event) => {
        this._data = event.data;
        this._dragging = true;

        if (this.canSelect()) {
            // clear activity controls
            this.getObjectsByType<Activity>(Activity).forEach(x => {
                x.hideControls();
            });

            if (!this._group.isDragging) {
                this._group.deselect();
                const newPosition = this._data.getLocalPosition(this.viewport);
                this._selectorStartX = newPosition.x;
                this._selectorStartY = newPosition.y;
                this._selector.x = newPosition.x;
                this._selector.y = newPosition.y;
                this._selector.visible = true;
            }
            else {
                this._dragging = false;
            }
        }
        else if (this.isGrabbing()) {
            document.querySelector("body").style.cursor = "grabbing";
        }
    };

    private _onDragMove = (event) => {
        if (this._dragging) {
            if (this.canSelect()) {

                // this._selector.visible = true;
                const newPosition = this._data.getLocalPosition(this.viewport);
                this._selector.width = newPosition.x - this._selectorStartX;
                this._selector.height = newPosition.y - this._selectorStartY;

            }
            else {

            }
        }
    };

    private _onDragEnd = (event) => {
        if (this._dragging) {
            if (this.canSelect()) {
                if (!this._group.isDragging) {
                    let selectedObjects = PixiApp.getInstance().viewport.children
                        .filter(x => (x instanceof Activity
                            || x instanceof ActivityPathHandle)
                            && !(x instanceof Background)
                            && Collision.hitTestRectangle(this._selector, x));
                    GroupSelection.getInstance().setGrouping(selectedObjects as Graphics[]);
                    this._selector.visible = false;
                    this._selector.width = 0;
                    this._selector.height = 0;
                }
            }
            else if (this.isGrabbing()) {
                document.querySelector("body").style.cursor = "grab";
            }
        }

        this._dragging = false;
        this._data = null;
    };
}
