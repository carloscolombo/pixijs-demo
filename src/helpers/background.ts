import { Collision } from './collision';
import { Graphics } from "src/models/graphics";
import { PixiApp } from './pixiApp';
import { GroupSelection } from "./group";
import * as PIXI from 'pixi.js';

export class Background extends PIXI.TilingSprite {
    private static instance: Background;

    private constructor(texture: PIXI.Texture) {
        super(texture, window.innerWidth, window.innerHeight);
    }

    public static getInstance(): Background {
        if (!Background.instance) {
            let texture = PIXI.Texture.from('/assets/tile-1.png');
            Background.instance = new Background(texture);
            Background.instance.alpha = .15;
        }
        return Background.instance;
    }
}
