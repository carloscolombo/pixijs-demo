import { Component, OnInit } from '@angular/core';
import * as PIXI from 'pixi.js'
import { Activity } from 'src/models/activity';
import { ActivityService } from 'src/services/activity.service';
import { PixiApp, PixiAppState } from "src/helpers/pixiApp";
import { Coordinates } from 'src/models/activity-path';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pixijs-demo';

  selectedActivity;

  constructor(private _activityService: ActivityService) { }

  ngOnInit() {
    let app = PixiApp.getInstance();
    document.body.appendChild(app.view);

    for (let index = 0; index < 1; index++) {
      const element = 100;
      this.addIf();
    }

    PixiApp.getInstance().state.subscribe(state => {
      if (state.selectedActivity) {
        this.selectedActivity = state.selectedActivity
      } else {
        this.selectedActivity = null;
      }
    });
  }

  addIf() {
    this._activityService.createIf(100, 100);

    this._activityService.createIf(150, 300);
  }
}
