import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitySelectionOptionsComponent } from './activity-selection-options.component';

describe('ActivitySelectionOptionsComponent', () => {
  let component: ActivitySelectionOptionsComponent;
  let fixture: ComponentFixture<ActivitySelectionOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitySelectionOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitySelectionOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
