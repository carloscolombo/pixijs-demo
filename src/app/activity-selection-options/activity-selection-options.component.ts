import { Component, OnInit, Input, ChangeDetectionStrategy, SimpleChanges } from '@angular/core';
import { PixiAppState } from 'src/helpers/pixiApp';
import { Coordinates, Size } from 'src/models/activity-path';

@Component({
  selector: 'activity-selection-options',
  templateUrl: './activity-selection-options.component.html',
  styleUrls: ['./activity-selection-options.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivitySelectionOptionsComponent implements OnInit {

  @Input() coordinates: Coordinates;
  @Input() size: Size;

  get left() { return `${this.coordinates.x}px`; }
  get top() { return `${this.coordinates.y + this.size.height}px`; }

  constructor() { }

  ngOnInit() {
  }
}
