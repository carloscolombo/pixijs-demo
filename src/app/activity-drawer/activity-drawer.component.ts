import { Component, OnInit } from '@angular/core';
import { Background } from "src/helpers/background";
import { PixiApp, AppState } from "src/helpers/pixiApp";


@Component({
  selector: 'app-activity-drawer',
  templateUrl: './activity-drawer.component.html',
  styleUrls: ['./activity-drawer.component.css']
})
export class ActivityDrawerComponent implements OnInit {
  pixiApp: PixiApp;

  get isPointerState() { return this.pixiApp.appState == AppState.POINTER; }
  get isGrabState() { return this.pixiApp.appState == AppState.GRAB; }
  get isSelectorState() { return this.pixiApp.appState == AppState.SELECTOR; }

  constructor() { }

  ngOnInit() {
    this.pixiApp = PixiApp.getInstance();
  }

  selecting() {
    document.querySelector("body").style.cursor = "default";
    this.pixiApp.setSelectorState();
  }

  pointer() {
    document.querySelector("body").style.cursor = "default";
    this.pixiApp.setPointerState();
  }

  grab() {
    document.querySelector("body").style.cursor = "grab";
    this.pixiApp.setGrabState();
  }
}
