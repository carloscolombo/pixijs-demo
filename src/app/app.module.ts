import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ActivityService } from 'src/services/activity.service';
import { ActivityDrawerComponent } from './activity-drawer/activity-drawer.component';
import { ActivityDrawerItemComponent } from './activity-drawer-item/activity-drawer-item.component';
import { ZoomControlsComponent } from './zoom-controls/zoom-controls.component';
import { ActivitySelectionOptionsComponent } from './activity-selection-options/activity-selection-options.component';

@NgModule({
  declarations: [
    AppComponent,
    ActivityDrawerComponent,
    ActivityDrawerItemComponent,
    ZoomControlsComponent,
    ActivitySelectionOptionsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    ActivityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
