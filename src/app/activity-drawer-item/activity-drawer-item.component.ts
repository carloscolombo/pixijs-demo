import { Component, OnInit } from '@angular/core';
import uuidv1 from 'uuid/v1';
import { PixiApp } from "src/helpers/pixiApp";
import { ActivityService } from 'src/services/activity.service';

@Component({
  selector: 'activity-drawer-item',
  templateUrl: './activity-drawer-item.component.html',
  styleUrls: ['./activity-drawer-item.component.css']
})
export class ActivityDrawerItemComponent implements OnInit {

  id = uuidv1();
  dragging = false;

  constructor(private _activityService: ActivityService) { }

  ngOnInit() { }

  ngAfterViewInit() {
    let item = document.getElementById(this.id);

    item.addEventListener('mousedown', (event) => {
      // this._activityService.createIf()
      let rect = (<HTMLTextAreaElement>event.target)
        .closest('.activity-drawer-container')
        .getBoundingClientRect();

      this._activityService.createIf(10, (rect.top - 100));
    });
  }

}
