import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityDrawerItemComponent } from './activity-drawer-item.component';

describe('ActivityDrawerItemComponent', () => {
  let component: ActivityDrawerItemComponent;
  let fixture: ComponentFixture<ActivityDrawerItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityDrawerItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDrawerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
