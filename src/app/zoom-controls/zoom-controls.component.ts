import { Component, OnInit } from '@angular/core';
import { PixiApp } from 'src/helpers/pixiApp';
import { Activity } from 'src/models/activity';

@Component({
  selector: 'zoom-controls',
  templateUrl: './zoom-controls.component.html',
  styleUrls: ['./zoom-controls.component.css']
})
export class ZoomControlsComponent implements OnInit {

  private zoomPercentage = 1.0;

  get percentage() {
    return `${(this.zoomPercentage * 100).toFixed(0)}`;
  }

  constructor() { }

  ngOnInit() {
  }

  zoomIn() {
    let pixiApp = PixiApp.getInstance();

    pixiApp.zoom = pixiApp.zoom + .20;
    this.zoomPercentage = pixiApp.zoom;

    pixiApp.viewport.setZoom(this.zoomPercentage);

    pixiApp.getObjectsByType<Activity>(Activity).forEach(x => {
      x.updateControls();
    });
  }

  zoomOut() {
    let pixiApp = PixiApp.getInstance();

    pixiApp.zoom = pixiApp.zoom - .20;
    this.zoomPercentage = pixiApp.zoom;

    pixiApp.viewport.setZoom(this.zoomPercentage);

    pixiApp.getObjectsByType<Activity>(Activity).forEach(x => {
      x.updateControls();
    });
  }

}
